import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const testUserData = {
    email: `test-${uuidv4()}@example.com`,
    password: 'TestPassword123',
    userName: `testUser-${uuidv4()}`,
    avatar: 'http://example.com/avatar.jpg'
};

describe('User Login', function() {
    const invalidCredentialsDataSet = [
        {email:  "wrong_" + testUserData.email, password: testUserData.password},
        {email:  "wrong_address" + testUserData.email, password: testUserData.password},
        {email:  "wrong_address" + testUserData.email, password: 'NOT_MY_PASSWORD'},

    ];
    const wrongEmail = "wrong_" + testUserData.email;
    const authController = new AuthController();
    const registerController = new RegisterController();
    before(async function() {
        let response = await registerController.registerUser(testUserData);
        checkStatusCode(response, 201);
    });

    invalidCredentialsDataSet.forEach((invalidCredentials) =>{
        it('should not login with invalid credentials', async function() {
            const response = await authController.login(invalidCredentials.email, invalidCredentials.password);
            console.log(response.body);
            expect(response.body).to.have.property('error');
            expect(response.body).to.have.property('code');
            expect(response.body.code).to.equal(2);
            expect(response.statusCode).to.equal(404); // Changed from 'status' to 'statusCode'
        });
    })

});
