import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from '../lib/controllers/register.controller';
import { UsersController } from '../lib/controllers/users.controller';
import { PostsController } from '../lib/controllers/posts.controller'; // Додавання імпорту контролера постів
import { checkStatusCode } from '../../helpers/functionsForChecking.helper';

describe('Create New Post API Test', () => {
  const authController = new AuthController();
  const registerController = new RegisterController();
  const usersController = new UsersController();
  const postsController = new PostsController(); // Ініціалізація контролера постів
  let token: string;
  let userId: number;

  before(async () => {
    // Register a new user
    const newUser = {
      email: `test-${uuidv4()}@example.com`,
      password: 'TestPassword123',
      userName: `testUser-${uuidv4()}`,
      avatar: 'http://example.com/avatar.jpg'
    };
    const registerResponse = await registerController.registerUser(newUser);
    checkStatusCode(registerResponse, 201);

    // Login to get the access token
    const loginResponse = await authController.login(newUser.email, newUser.password);
    token = loginResponse.body.token.accessToken.token;

    // Get the user ID
    const currentUserResponse = await usersController.getCurrentUser(token);
    userId = currentUserResponse.body.id;
  });

  it('should create a new post successfully', async () => {
    // Data for creating a new post
    const newPostData = {
      authorId: userId,
      previewImage: 'http://example.com/image.jpg',
      body: 'New post body'
    };

    // Create a new post
    const createPostResponse = await postsController.createPost(newPostData, token);
    checkStatusCode(createPostResponse, 200);

    // Assert
    expect(createPostResponse.body).to.have.property('id');
    expect(createPostResponse.body.author.id).to.equal(userId);
    expect(createPostResponse.body.previewImage).to.equal(newPostData.previewImage);
    expect(createPostResponse.body.body).to.equal(newPostData.body);
  });
});
