import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from '../lib/controllers/register.controller';
import { UsersController } from '../lib/controllers/users.controller';
import { PostsController } from '../lib/controllers/posts.controller';
import { checkStatusCode } from '../../helpers/functionsForChecking.helper';

describe('Add Like Reaction to Post API Test', () => {
  const authController = new AuthController();
  const registerController = new RegisterController();
  const usersController = new UsersController();
  const postsController = new PostsController();
  let token: string;
  let userId: number;
  let postId: number;

  before(async () => {
    // Register a new user
    const newUser = {
      email: `test-${uuidv4()}@example.com`,
      password: 'TestPassword123',
      userName: `testUser-${uuidv4()}`,
      avatar: 'http://example.com/avatar.jpg'
    };
    const registerResponse = await registerController.registerUser(newUser);
    checkStatusCode(registerResponse, 201);

    // Login to get the access token
    const loginResponse = await authController.login(newUser.email, newUser.password);
    token = loginResponse.body.token.accessToken.token;

    // Get the user ID
    const currentUserResponse = await usersController.getCurrentUser(token);
    userId = currentUserResponse.body.id;

    // Create a new post
    const newPostData = {
      authorId: userId,
      previewImage: 'http://example.com/image.jpg',
      body: 'New post body'
    };
    const createPostResponse = await postsController.createPost(newPostData, token);
    checkStatusCode(createPostResponse, 200);
    postId = createPostResponse.body.id;
  });

  it('should add a like reaction to the post successfully', async () => {
    // Data for adding like reaction to the post
    const newReactionData = {
      entityId: postId,
      isLike: true,
      userId: userId
    };

    // Add a like reaction to the post
    const addReactionResponse = await postsController.addLikeReaction(newReactionData, token);
    checkStatusCode(addReactionResponse, 200);
  });
});
