import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from '../lib/controllers/register.controller';
import { UsersController } from '../lib/controllers/users.controller';
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const testUserData = {
  email: `test-${uuidv4()}@example.com`,
  password: 'TestPassword123',
  userName: `testUser-${uuidv4()}`,
  avatar: 'http://example.com/avatar.jpg'
};

describe('Get User by Id API Test', () => {
  let token: string;
  let userId: number;
  const authController = new AuthController();
  const registerController = new RegisterController();
  const usersController = new UsersController();

  before(async () => {
    // User registration
    let response = await registerController.registerUser(testUserData);
    checkStatusCode(response, 201);

    // Authorization to obtain token
    const loginResponse = await authController.login(testUserData.email, testUserData.password);
    token = loginResponse.body.token.accessToken.token;

    // Get the user ID
    userId = response.body.user.id;
  });

  it('should get user details by id', async () => {
    // Act
    const userDetailsResponse = await usersController.getUserById(userId);

    // Assert
    checkStatusCode(userDetailsResponse, 200);
    expect(userDetailsResponse.body.email).to.equal(testUserData.email);
    expect(userDetailsResponse.body.userName).to.equal(testUserData.userName);
    // Add more assertions if needed
  });

});
