import { expect } from "chai";
import { RegisterController } from '../lib/controllers/register.controller';
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();

// Function to generate a random string
function generateRandomString(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

describe("User Registration", () => {
    const randomString = generateRandomString(5);
    const newUser = {
        avatar: "http://example.com/avatar.jpg",
        email: `${randomString}@example.com`,
        userName: `user${randomString}`,
        password: "Password123"
    };

    before(async () => {
        const response = await register.registerUser(newUser);
        checkStatusCode(response, 201);
    });

    it("should not register a new user with the same email", async () => {
        const response = await register.registerUser(newUser);

        // Assertions
        expect(response.body).to.have.property('error');
        expect(response.body).to.have.property('code');
    });

});
