import { expect } from 'chai';
import { PostsController } from '../lib/controllers/posts.controller';
import { checkStatusCode } from '../../helpers/functionsForChecking.helper';

describe('Get All Posts API Test', () => {
  const postsController = new PostsController();

  it('should get all posts successfully', async () => {
    // Arrange
    // No specific arrangements needed for this test

    // Act
    const response = await postsController.getAllPosts();

    // Assert
    // Checking that the status code of the response is 200
    checkStatusCode(response, 200);

    // Checking that the response body is an array
    expect(response.body).to.be.an('array');

    // Additional checks can be added to validate the structure of each post
    // For example, checking that each post has required fields such as id, createdAt, author, previewImage, body, comments, reactions, etc.
    // Add additional checks based on your requirements and needs

    // Example check for the structure of the first post
    const firstPost = response.body[0];
    expect(firstPost).to.have.property('id');
    expect(firstPost).to.have.property('createdAt');
    expect(firstPost).to.have.property('author');
    expect(firstPost).to.have.property('previewImage');
    expect(firstPost).to.have.property('body');
    expect(firstPost).to.have.property('comments');
    expect(firstPost).to.have.property('reactions');

    // Additional checks can be added based on your requirements
  });
});
