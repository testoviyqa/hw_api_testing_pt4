import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from '../lib/controllers/register.controller';
import { UsersController } from '../lib/controllers/users.controller';
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const testUserData = {
  email: `test-${uuidv4()}@example.com`,
  password: 'TestPassword123',
  userName: `testUser-${uuidv4()}`,
  avatar: 'http://example.com/avatar.jpg'
};

describe('Delete Current User API Test', () => {
  let token: string;
  let userId: number;
  const authController = new AuthController();
  const registerController = new RegisterController();
  const usersController = new UsersController();

  it('should delete the current user', async () => {
    // User registration
    let response = await registerController.registerUser(testUserData);
    checkStatusCode(response, 201);

    // Authorization to obtain token
    const loginResponse = await authController.login(testUserData.email, testUserData.password);
    token = loginResponse.body.token.accessToken.token;

    // Getting current user ID
    const currentUserResponse = await usersController.getCurrentUser(token);
    userId = currentUserResponse.body.id;

    // Delete current user
    const deleteResponse = await registerController.deleteUser(userId, token);
    checkStatusCode(deleteResponse, 204);

    // Try to get user details after deletion
    const getUserResponse = await usersController.getUserById(userId);
    
    // Assert
    checkStatusCode(getUserResponse, 404);
  });
});

