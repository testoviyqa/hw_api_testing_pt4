import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from '../lib/controllers/register.controller';
import { UsersController } from '../lib/controllers/users.controller';
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const testUserData = {
  email: `test-${uuidv4()}@example.com`,
  password: 'TestPassword123',
  userName: `testUser-${uuidv4()}`,
  avatar: 'http://example.com/avatar.jpg'
};

describe('Combined User API Tests', () => {
  let token: string;
  const authController = new AuthController();
  const registerController = new RegisterController();
  const usersController = new UsersController();

  before(async () => {
    // User registration
    let response = await registerController.registerUser(testUserData);
    checkStatusCode(response, 201);

    // Authorization to obtain token
    const loginResponse = await authController.login(testUserData.email, testUserData.password);
    token = loginResponse.body.token.accessToken.token;
  });

  it('should update the current user successfully and then get updated data', async () => {
    // Arrange
    const currentUserResponse = await usersController.getCurrentUser(token);
    checkStatusCode(currentUserResponse, 200);
    let userDataToUpdate = currentUserResponse.body;
    
    // Act
    userDataToUpdate.userName = 'updated_username_' + userDataToUpdate.userName;
    const updateResponse = await usersController.updateUser(userDataToUpdate, token);
    checkStatusCode(updateResponse, 204);

    // Assert
    const updatedUserResponse = await usersController.getCurrentUser(token);
    checkStatusCode(updatedUserResponse, 200);
    expect(updatedUserResponse.body.userName).to.equal(userDataToUpdate.userName);
    expect(updatedUserResponse.body.email).to.equal(testUserData.email);
  });

});
