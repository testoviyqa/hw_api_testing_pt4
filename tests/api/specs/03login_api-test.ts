import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const testUserData = {
    email: `test-${uuidv4()}@example.com`,
    password: 'TestPassword123',
    userName: `testUser-${uuidv4()}`,
    avatar: 'http://example.com/avatar.jpg'
};

describe('User Login', function() {
    const authController = new AuthController();
    const registerController = new RegisterController();
    before(async function() {
        let response = await registerController.registerUser(testUserData);
        checkStatusCode(response, 201);
    });

    it('should login successfully with valid credentials', async function() {
        const response = await authController.login(testUserData.email, testUserData.password);
        expect(response.statusCode).to.equal(200); // Changed from 'status' to 'statusCode'
        expect(response.body).to.have.property('token');
    });
});
