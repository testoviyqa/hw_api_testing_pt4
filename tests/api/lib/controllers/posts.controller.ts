import { ApiRequest } from "../request";

export class PostsController {
    async getAllPosts() {
        // Using the GET method to retrieve all posts
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl) // Adding prefixUrl
            .method("GET")
            .url(`api/Posts`)
            .send();
        
        // Returning the response object
        return response;
    }

    async createPost(postData: any, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method("POST")
            .bearerToken(accessToken)
            .url(`api/Posts`)
            .body(postData)
            .send();
        
        return response;
    }

    async addLikeReaction(reactionData: any, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method("POST")
            .bearerToken(accessToken)
            .url(`api/Posts/like`)
            .body(reactionData)
            .send();
        
        return response;
    }

    async addComment(commentData: any, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method("POST")
            .bearerToken(accessToken)
            .url(`api/Comments`)
            .body(commentData)
            .send();
        
        return response;
    }
}

