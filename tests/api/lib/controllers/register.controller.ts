import { ApiRequest } from "../request";

interface UserRegistrationData {
    id?: number; // Тепер 'id' є необов'язковим
    avatar: string;
    email: string;
    userName: string;
    password: string;
}

export class RegisterController {
    async registerUser(userData: UserRegistrationData) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(userData)
            .send();
        return response;
    }

    async deleteUser(userId: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(global.appConfig.baseUrl)
            .method("DELETE")
            .bearerToken(accessToken)
            .url(`api/Users/${userId}`)
            .send();
        return response;
    }
}
